<div align="center">

# Sistemas Distribuidos UBU

[![Project](https://img.shields.io/badge/Project-University-blueviolet.svg)][repo-link]
[![Repository](https://img.shields.io/badge/gitlab-purple?logo=gitlab)][repo-link]
[![Language](https://img.shields.io/badge/Java-00718B?logo=java)][java-link]
[![Original-Repository](https://img.shields.io/badge/bitbucket-Original_Repository-0747A6?logo=bitbucket)][original-repo-link]

Prácticas de la asignatura de Sistemas Distribuidos de 4º curso
del grado de Ingeniería Informática de la Universidad de Burgos.

</div>
<hr>

## Built with

### Technologies

[<img src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/java/java.png" width=50 alt="Java">][java-link]

### Platforms

[<img src="https://raw.githubusercontent.com/BorjaG90/media/master/img/logos/eclipse.png" width=50 alt="Eclipse">][eclipse-link]


<div align="center">

## Authors

### **Borja Gete**

[![Mail](https://img.shields.io/badge/borjag90dev@gmail.com-DDDDDD?style=for-the-badge&logo=gmail)][borjag90dev-gmail]
[![Github](https://img.shields.io/badge/BorjaG90-000000.svg?&style=for-the-badge&logo=github&logoColor=white)][borjag90dev-github]
[![Gitlab](https://img.shields.io/badge/BorjaG90-purple.svg?&style=for-the-badge&logo=gitlab)][borjag90dev-gitlab]
[![LinkedIn](https://img.shields.io/badge/borjag90-0077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)][borjag90dev-linkedin]

### **Plamen Petkov**

[![Mail](https://img.shields.io/badge/petkov092@gmail.com-DDDDDD?style=for-the-badge&logo=gmail)][plamen-gmail]
[![BitBucket](https://img.shields.io/badge/Plamen_Petkov-0747A6.svg?&style=for-the-badge&logo=bitbucket)][plamen-bitbucket]
[![LinkedIn](https://img.shields.io/badge/Plamen_Petkov-0077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white)][plamen-linkedin]


</div>

[borjag90dev-gmail]: mailto:borjag90dev@gmail.com
[borjag90dev-github]: https://github.com/BorjaG90
[borjag90dev-gitlab]: https://gitlab.com/BorjaG90
[borjag90dev-linkedin]: https://www.linkedin.com/in/borjag90/
[plamen-gmail]: mailto:petkov092@gmail.com
[plamen-linkedin]: https://www.linkedin.com/in/plamen-petkov/
[plamen-bitbucket]: https://bitbucket.org/ppp0015/
[original-repo-link]: https://BorjaG90@bitbucket.org/BorjaG90/practicas_distribuidos.git
[repo-link]: https://gitlab.com/bg90dev-ubu/ubu-practicas-distribuidos
[eclipse-link]: https://www.eclipse.org/downloads/
[java-link]: https://www.java.com/es/
